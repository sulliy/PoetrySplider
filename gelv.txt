绝句律诗格律示例	
五绝类型一 五绝类型二 五绝类型三 五绝类型四 七绝类型一 七绝类型二 七绝类型三 七绝类型四 五律类型一 五律类型二 五律类型三 五律类型四 七律类型一 七律类型二 七律类型三 七律类型四	
⊙表示可平可仄	
五绝类型一 	
⊙平平仄仄， ⊙仄仄平平。（韵） ⊙仄平平仄， 平平仄仄平。（韵） 	
山中	
王勃	
长江悲已滞， 万里念将归。 况属高秋晚， 山中黄叶飞。 	
五绝类型二 	
平平仄仄平，（韵） ⊙仄仄平平。（韵） ⊙仄平平仄， 平平仄仄平。（韵） 	
静夜思	
李白	
床前明月光， 疑是地上霜。 举头望明月， 低头思故乡。 	
五绝类型三 	
⊙仄平平仄， 平平仄仄平。（韵） ⊙平平仄仄， ⊙仄仄平平。（韵） 	
南行别第	
韦承庆	
万里人南去， 三春雁北飞。 不知何岁月， 得与尔同归。 	
五绝类型四 	
⊙仄仄平平，（韵） 平平仄仄平。（韵） ⊙平平仄仄， ⊙仄仄平平。（韵） 	
塞下曲	
卢纶	
林暗草惊风， 将军夜引弓。 平明寻白羽， 没在石棱中。	
七绝类型一 	
⊙平⊙仄平平仄， ⊙仄平平仄仄平。（韵） ⊙仄⊙平平仄仄， ⊙平⊙仄仄平平。（韵） 	
南游感兴	
窦巩	
伤心欲问前朝事， 惟见江流去不回。 日暮东风春草绿， 鹧鸪飞上越王台。 	
七绝类型二 	
⊙平⊙仄仄平平，（韵） ⊙仄平平仄仄平。（韵） ⊙仄⊙平平仄仄， ⊙平⊙仄仄平平。（韵） 	
出塞 	
王昌龄	
秦时明月汉时关， 万里长征人未还。 但使龙城飞将在， 不教胡马度阴山。 	
七绝类型三 	
⊙仄⊙平平仄仄， ⊙平⊙仄仄平平。（韵） ⊙平⊙仄平平仄， ⊙仄平平仄仄平。（韵） 	
九月九日忆山东兄弟	
王维	
独在异乡为异客， 每逢佳节倍思亲。 遥知兄弟登高处， 遍插茱萸少一人。 	
七绝类型四 	
⊙仄平平仄仄平，（韵） ⊙平⊙仄仄平平。（韵） ⊙平⊙仄平平仄， ⊙仄平平仄仄平。（韵） 	
从军行	
王昌龄	
青海长云暗雪山， 孤城遥望玉门关。 黄沙百战穿金甲， 不破楼兰终不还！ 	
五律类型一 	
⊙平⊙仄仄 ⊙仄仄平平（韵） ⊙仄⊙平仄 平平⊙仄平（韵） ⊙平⊙仄仄 ⊙仄仄平平（韵） ⊙仄⊙平仄 平平⊙仄平（韵） 	
送友人	
李白	
青山横北郭， 白水绕孤城。 此地一为别， 孤蓬万里征。 浮云游子意， 落日故人情。 挥手自兹去， 萧萧班马鸣。 	
五律类型二 	
平平⊙仄平（韵） ⊙仄仄平平（韵） ⊙仄⊙平仄 平平⊙仄平（韵） ⊙平⊙仄仄 ⊙仄仄平平（韵） ⊙仄⊙平仄 平平⊙仄平（韵） 	
晚晴	
李商隐	
深居俯夹城， 春去夏犹清。 天意怜幽草， 人间重晚晴。 并添高阁回， 微注小窗明。 越鸟巢干后， 归飞体更轻。 	
五律类型三 	
⊙仄⊙平仄 平平⊙仄平（韵） ⊙平⊙仄仄 ⊙仄仄平平（韵） ⊙仄⊙平仄 平平⊙仄平（韵） ⊙平⊙仄仄 ⊙仄仄平平（韵） 	
杜甫	
春望	
国破山河在， 城春草木深。 感时花溅泪， 恨别鸟惊心。 烽火连三月， 家书抵万金。 白头搔更短， 浑欲不胜簪。 	
五律类型四 	
⊙仄仄平平（韵） 平平⊙仄平（韵） ⊙平⊙仄仄 ⊙仄仄平平（韵） ⊙仄⊙平仄 平平⊙仄平（韵） ⊙平⊙仄仄 ⊙仄仄平平（韵） 	
终南山	
王维	
太乙近天都， 连山到海隅。 白云回望合， 青霭入看无。 分野中峰变， 阴晴众壑珠。 欲投人处宿， 隔水问樵夫。 	
七律类型一 	
⊙平⊙仄⊙平仄 ⊙仄平平⊙仄平（韵） ⊙仄⊙平⊙仄仄 ⊙平⊙仄仄平平（韵） ⊙平⊙仄⊙平仄 ⊙仄平平⊙仄平（韵） ⊙仄⊙平⊙仄仄 ⊙平⊙仄仄平平（韵） 	
客至	
杜甫	
舍南舍北皆春水， 但见群鸥日日来。 花径不曾缘客扫， 蓬门今始为君开。 盘飧市远无兼味， 樽酒家贫只旧醅。 肯与邻翁相对饮， 隔篱呼取尽余杯。 	
七律类型二 	
⊙平⊙仄仄平平（韵） ⊙仄平平⊙仄平（韵） ⊙仄⊙平⊙仄仄 ⊙平⊙仄仄平平（韵） ⊙平⊙仄⊙平仄 ⊙仄平平⊙仄平（韵） ⊙仄⊙平⊙仄仄 ⊙平⊙仄仄平平（韵） 	
左迁蓝关示侄孙湘	
韩愈	
一封朝奏九重天， 夕贬潮州路八千。 欲为圣明除弊事， 肯将衰朽惜残年。 云横秦岭家何在？ 雪拥蓝关马不前。 知汝远来应有意， 好收吾骨瘴江边。 	
七律类型三 	
⊙仄⊙平⊙仄仄 ⊙平⊙仄仄平平（韵） ⊙平⊙仄⊙平仄 ⊙仄平平⊙仄平（韵） ⊙仄⊙平⊙仄仄 ⊙平⊙仄仄平平（韵） ⊙平⊙仄⊙平仄 ⊙仄平平⊙仄平（韵） 	
咏怀古迹	
杜甫	
诸葛大名垂宇宙， 宗臣遗像肃清高。 三分割据纡筹策， 万古云霄一羽毛。 伯仲之间见伊吕， 指挥若定失萧曹。 运移汉祚终难复， 志决身歼军务劳。 	
七律类型四 	
⊙仄平平⊙仄平（韵） ⊙平⊙仄仄平平（韵） ⊙平⊙仄⊙平仄 ⊙仄平平⊙仄平（韵） ⊙仄⊙平⊙仄仄 ⊙平⊙仄仄平平（韵） ⊙平⊙仄⊙平仄 ⊙仄平平⊙仄平（韵） 	
登高	
杜甫	
风急天高猿啸哀， 渚清沙白鸟飞回。 无边落木萧萧下， 不尽长江滚滚来。 万里悲秋常作客， 百年多病独登台。 艰难苦恨繁霜鬓， 潦倒新停浊酒杯。	
选自诗神站点	
灵石岛制作	
