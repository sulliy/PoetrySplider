package com.changeword.spider.core;

import java.util.List;

import com.changeword.spider.bean.LinkTypeData;
import com.changeword.spider.rule.Rule;
import com.changeword.spider.util.FileUtil;

public class Main {

	public static void main(String args[]){
		Rule rule1 = new Rule("http://www.shigeku.org/xlib/lingshidao/gushi/index.htm",  
				null, null,  
				"a", Rule.SELECTION, Rule.GET);  
		        
		List<LinkTypeData> extracts = ExtractService.filterLink(ExtractService.extract2(rule1)); 
		
		printf(extracts); 
		
		//downloadAsFile();
	}

	private static void downloadAsFile(String name) {
		Rule rule2 = new Rule("http://www.shigeku.org/xlib/lingshidao/gushi/" + name + ".htm",  
                "body>p", Rule.SELECTION,Rule.CHAR_SET_GBK); 
		List<String> ps = ExtractService.filterText(ExtractService.extract1(rule2));
		FileUtil.writeTxtFile(ps, name + ".txt");
	}
	
	public static void printf(List<LinkTypeData> datas)  
    {  
        for (LinkTypeData data : datas)  
        {  
        	if(!(data.getLinkHref().contains("http") || (data.getLinkHref().contains("/")))){
	            System.out.println("Start download " + data.getLinkText());  
	            downloadAsFile(data.getLinkHref().subSequence(0, data.getLinkHref().indexOf('.')).toString());  
	            System.out.println("***********************************");  
        	}
        }  
  
    } 
}
