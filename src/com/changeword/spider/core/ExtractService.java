package com.changeword.spider.core;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.changeword.spider.bean.LinkTypeData;
import com.changeword.spider.rule.Rule;
import com.changeword.spider.rule.RuleException;
import com.changeword.spider.util.TextUtil;

/**
 * 
 * @author zhy
 * 
 */
public class ExtractService
{
	public static Elements extract1(Rule rule)
	{
		validateRule(rule);

		Elements results = new Elements();
		try
		{
			String url = rule.getUrl();
			String resultTagName = rule.getResultTagName();
			int type = rule.getType();
			String charSet = rule.getCharSet();

			Document doc = Jsoup.parse(new URL(url).openStream(), charSet, url); 

			switch (type)
			{
			case Rule.CLASS:
				results = doc.getElementsByClass(resultTagName);
				break;
			case Rule.ID:
				Element result = doc.getElementById(resultTagName);
				results.add(result);
				break;
			case Rule.SELECTION:
				results = doc.select(resultTagName);
				break;
			default:
				//当resultTagName为空时默认去body标签
				if (TextUtil.isEmpty(resultTagName))
				{
					results = doc.getElementsByTag("body");
				}
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return results;
	}
	
	public static Elements extract2(Rule rule)
	{
		validateRule(rule);

		Elements results = new Elements();
		try
		{
			String url = rule.getUrl();
			String[] params = rule.getParams();
			String[] values = rule.getValues();
			String resultTagName = rule.getResultTagName();
			int type = rule.getType();
			int requestType = rule.getRequestMoethod();

			Connection conn = Jsoup.connect(url);
			
			if (params != null)
			{
				for (int i = 0; i < params.length; i++)
				{
					conn.data(params[i], values[i]);
				}
			}

			Document doc = null;
			switch (requestType)
			{
			case Rule.GET:
				doc = conn.timeout(100000).get();
				break;
			case Rule.POST:
				doc = conn.timeout(100000).post();
				break;
			}
			
			switch (type)
			{
			case Rule.CLASS:
				results = doc.getElementsByClass(resultTagName);
				break;
			case Rule.ID:
				Element result = doc.getElementById(resultTagName);
				results.add(result);
				break;
			case Rule.SELECTION:
				results = doc.select(resultTagName);
				break;
			default:
				//当resultTagName为空时默认去body标签
				if (TextUtil.isEmpty(resultTagName))
				{
					results = doc.getElementsByTag("body");
				}
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return results;
	}
	
	public static List<LinkTypeData> filterLink(Elements elements){
		List<LinkTypeData> datas = new ArrayList<LinkTypeData>();
		LinkTypeData data = null;
		for (Element result : elements){
			Elements links = result.getElementsByTag("a");

			for (Element link : links)
			{
				//必要的筛选
				String linkHref = link.attr("href");
				String linkText = link.text();

				data = new LinkTypeData();
				data.setLinkHref(linkHref);
				data.setLinkText(linkText);

				datas.add(data);
			}
		}
		
		return datas;
	}
	
	public static List<String> filterText(Elements elements) {
		List<String> texts = new ArrayList<String>();
		for (Element element : elements){
			if(element.text() != null && element.text().length() > 0){
				texts.add(element.text());
			}
		}
		return texts;
	}

	/**
	 * 对传入的参数进行必要的校验
	 */
	private static void validateRule(Rule rule)
	{
		String url = rule.getUrl();
		if (TextUtil.isEmpty(url))
		{
			throw new RuleException("url不能为空！");
		}
		if (!url.startsWith("http://"))
		{
			throw new RuleException("url的格式不正确！");
		}

		if (rule.getParams() != null && rule.getValues() != null)
		{
			if (rule.getParams().length != rule.getValues().length)
			{
				throw new RuleException("参数的键值对个数不匹配！");
			}
		}

	}
}
