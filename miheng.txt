祢衡赋选	
祢衡(173～198)，汉末辞赋家。字正平。平原般(今山东临邑)人。少有才辩，性格刚毅傲慢，好侮慢权贵。因拒绝曹操召见，操怀忿，因其有才名，不欲杀之，罚作鼓史，祢衡则当众裸身击鼓，反辱曹操。曹操怒，欲借人手杀之，因遣送与荆州牧刘表。仍不合，又被刘表转送与江夏太守黄祖。后因冒犯黄祖，终被杀。《隋书·经籍志》有《祢衡集》2卷,久佚。今存文、赋见严可均《全上古三代秦汉三国六朝文》。　　　　　　　　　　　　　　　　　　　　	
鹦鹉赋	
鹦鹉赋	
　　惟西域之灵鸟兮，挺自然之奇姿。体全精之妙质兮，合火德之明辉。性辩慧而能言兮，才聪明以识机。故其嬉游高峻，栖跱幽深。飞不妄集，翔必择林。绀趾丹嘴，绿衣翠矜。采采丽容，咬咬好音。虽同族于羽毛，固殊智而异心。配鸾皇而等美，焉比德于众禽！	
　　于是羡芳声之远畅，伟灵表之可嘉。命虞人于陇坻，诏伯益于流沙，跨昆仑而播戈，冠云霓而张罗。虽纲维之备设，终一目之所加。且其容止闲暇，守植安停。逼之不惧，抚之不惊。宁顺从以远害，不违迕以丧身。故献金者受赏，而伤肌者被刑。尔乃归穷委命，离群丧侣。闭以雕笼，剪其翅羽。流飘万里，崎岖重阻。踰岷越障，载罹寒暑。女辞家而适人，臣出身而事主。彼贤哲之逢患，犹栖迟以羁旅。矧禽鸟之微物，能驯拢以安处。眷西路而长怀，望故乡而延。忖陋体之腥臊，亦何劳于鼎俎？嗟禄命之衰薄，奚遭时之险巇？岂言语以阶乱，将不密以致危？痛母子之永隔，哀伉俪之生离。匪余年之足惜，悯众雏之无知。背蛮夷之下国，侍君子之光仪。惧名实之不副，耻才能之无奇。羡西都之沃壤，识苦乐之异宜。怀代越之悠思，故每言而称斯。	
　　若乃少昊司辰，蓐收整辔。严霜初降，凉风萧瑟。长吟远慕，哀鸣感类。音声凄以激扬，容貌惨以憔悴。闻之者悲伤，见之者陨泪。放臣为之屡叹，弃妻为之歔欷。感平身之游处，若壎篪之相须。何今日之两绝，若胡越之异区。顺笼槛以俯仰，窥户牖以踟躇。想昆仑之高岳，思邓林之扶疏。顾六翮之残毁，虽奋迅其焉如？心怀归而弗果，徒怨毒于一隅。苟竭心于所事，敢背惠而忘初！托轻鄙之微命，委陋贱于薄躯。期守死以抱德，甘尽辞以效愚。恃隆恩于既往，庶弥久而不渝。	
灵石岛制作	
